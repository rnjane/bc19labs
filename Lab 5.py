def power(a, b):
  if b == 0:
    return 1
  elif b%2 == 0:
    c = power(a, b/2)
    return c*c
  else:
    return a*power(a, b-1)