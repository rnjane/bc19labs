'''create an empty list'''
myList = []

'''Loop through the numbers 2000 and 3201, finding numbers divisible by 7, and not divisiblr by 5'''
for number in range(2000, 3201):
    if (number % 7 == 0) and (number % 5 != 0):

        '''Append the numbers to the created list, converting all to strring'''
        myList.append(str(number))

'''Print the list, appending a comma between all elements in the list'''
print(','.join(myList))
