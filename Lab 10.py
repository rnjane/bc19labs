'''Get input of the numbers'''
numbers = input()

'''Split the numbers using using commas, and assign them to a list'''
myList = numbers.split(",")

'''Create a tuple from the list'''
myTuple = tuple(myList)

'''Pring the list and the tuple'''
print(myList)
print(myTuple)