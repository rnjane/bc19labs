class MyClass():

    #Constructor: initializes our word(s) to an empty string
    def __init__(self):
        self.word = ""

    '''Function to get input from the console'''
    def getString(self):
        self.word = input()

    '''Function to print the accepted string'''
    def printString(self):
        print(self.word.upper())

'''Make an object of the class'''
Class1 = MyClass()

'''Test the getString function'''
Class1.getString()

'''Test the printString function'''
Class1.printString()
